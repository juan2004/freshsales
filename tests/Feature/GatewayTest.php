<?php

namespace Tests\Feature;

use Placetopay\Tangram\Exceptions\InvalidSettingException;
use Pabon\FreshsalesSdk\Gateway;
use Tests\TestCase;

class GatewayTest extends TestCase
{
    public function testTheTokenIsMandatory(): void
    {
        $this->expectException(InvalidSettingException::class);
        $this->expectExceptionMessage('The required option "token" is missing.');

        $resolver = new Gateway([
            'url' => 'https://dev.placetopay.com',
        ]);
    }

    public function testItValidatesLoginIsAString(): void
    {
        $this->expectException(InvalidSettingException::class);
        $this->expectExceptionMessage('The option "token" with value array is expected to be of type "string", but is of type "array".');

        $resolver = new Gateway([
            'token' => ['placetopay'],
            'url' => 'https://dev.placetopay.com',
        ]);
    }

    public function testTheUrlIsMandatory(): void
    {
        $this->expectException(InvalidSettingException::class);
        $this->expectExceptionMessage('The required option "url" is missing.');

        $resolver = new Gateway([
            'token' => 'placetopay',
        ]);
    }

    public function testItValidatesUrlIsAString(): void
    {
        $this->expectException(InvalidSettingException::class);
        $this->expectExceptionMessage('The option "url" with value array is expected to be of type "string", but is of type "array".');

        $resolver = new Gateway([
            'token' => 'placetopay',
            'url' => ['https://dev.placetopay.com'],
        ]);
    }
}
