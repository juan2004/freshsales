<?php

namespace Tests\Feature;

use PlacetoPay\Base\Constants\ReasonCodes;
use PlacetoPay\Base\Entities\Status;
use Pabon\FreshsalesSdk\Constants\ExceptionMessages;
use Pabon\FreshsalesSdk\Entities\LeadTransaction;
use Pabon\FreshsalesSdk\Exceptions\FreshsalesSdkException;
use Pabon\FreshsalesSdk\Gateway;
use PlacetoPay\Tangram\Mock\TestLogger;
use Tests\TestCase;

class OperationsTest extends TestCase
{
    private function gateway(): Gateway
    {
        return new Gateway([
            'token' => 'ikuKOdQWiFDUPOT8jCePyg',
            'url' => 'https://placetopay.freshsales.io',
        ]);
    }

    private function gatewayWithLog(TestLogger $logger): Gateway
    {
        return new Gateway([
            'token' => 'ikuKOdQWiFDUPOT8jCePyg',
            'url' => 'https://placetopay.freshsales.io',
            'logger' => [
                'via' => $logger,
                'path' => 'fake/path/server3ds-sdk_log',
            ],
        ]);
    }

    private function dataRequest(): LeadTransaction
    {
        return new LeadTransaction([
            'full_name' => 'juan doe',
            'email' => 'juan.doe@test.com',
            'mobile_number' => '3006719234',
            'company' => ['name' => 'Evertec Inc'],
            'job_title' => 'TI',
            'medium' => 'Portal autogestión',
            'id_lead' => '8019988769',
            'emails' => [],
            'work_number' => '3053347566',
            'address' => 'Cll 45 #50',
            'city' => 'Medellin',
            'state' => 'Antioquia',
            'zipcode' => '04',
            'subscription_status' => ['Correct'],
            'country' => 'Colombia',
            'lead_stage_id' => 2,
            'lead_reason_id' => 2,
            'lead_source_id' => 2,
            'campaign_id' => 1,
            'keyword' => 'example',
            'time_zone' => 'UTF USD',
            'facebook' => 'JuanPa...',
            'twitter' => '@juan.pa',
            'linkedin' => 'no se',
            'territory_id' => '3',
        ]);
    }

    private function getNewIdLead(): int
    {
        $gateway = $this->gateway();

        $response = $gateway->createLead($this->dataRequest());

        return json_decode($response->message)->lead->id;
    }

    public function testItCanCreateALeadSuccessful(): void
    {
        $gateway = $this->gateway();

        $response = $gateway->createLead($this->dataRequest());

        $this->assertInstanceOf(Status::class, $response);
        $this->assertObjectHasAttribute('status', $response, Status::ST_OK);
        $this->assertObjectHasAttribute('reason', $response, ReasonCodes::APPROVED_TRANSACTION);
        $this->assertObjectHasAttribute('message', $response);
        $this->assertObjectHasAttribute('date', $response, date('c'));
    }

    public function testItCannotCreateALeadFailed(): void
    {
        $gateway = $this->gateway();

        $response = $gateway->createLead(new LeadTransaction([
            'email' => 'juan@test.com',
            'mobile_number' => '3006719234', ]));

        $this->assertInstanceOf(Status::class, $response);
        $this->assertObjectHasAttribute('status', $response, Status::ST_FAILED);
        $this->assertObjectHasAttribute('reason', $response, ReasonCodes::INVALID_RESPONSE);
        $this->assertObjectHasAttribute('message', $response);
        $this->assertObjectHasAttribute('date', $response, date('c'));
    }

    public function testItCanViewALeadSuccessful(): void
    {
        $gateway = $this->gateway();

        $data = new LeadTransaction([
            'id_lead' => $this->getNewIdLead(),
        ]);

        $response = $gateway->viewLead($data);

        $this->assertInstanceOf(Status::class, $response);
        $this->assertObjectHasAttribute('status', $response, Status::ST_OK);
        $this->assertObjectHasAttribute('reason', $response, ReasonCodes::APPROVED_TRANSACTION);
        $this->assertObjectHasAttribute('message', $response);
        $this->assertObjectHasAttribute('date', $response, date('c'));
    }

    public function testItCannotViewALeadWithoutTheIdLead(): void
    {
        $gateway = $this->gateway();

        $data = new LeadTransaction([
        ]);

        $this->expectException(FreshsalesSdkException::class);
        $this->expectExceptionMessage(ExceptionMessages::ID_LEAD);

        $response = $gateway->viewLead($data);
    }

    public function testItCannotViewALeadFailed(): void
    {
        $gateway = $this->gateway();

        $response = $gateway->viewLead(new LeadTransaction([
            'id_lead' => 1,
        ]));

        $this->assertInstanceOf(Status::class, $response);
        $this->assertObjectHasAttribute('status', $response, Status::ST_FAILED);
        $this->assertObjectHasAttribute('reason', $response, ReasonCodes::INVALID_RESPONSE);
        $this->assertObjectHasAttribute('message', $response);
        $this->assertObjectHasAttribute('date', $response, date('c'));
    }

    public function testItCanViewALeadAndTheIncludeSuccessful(): void
    {
        $gateway = $this->gateway();

        $data = new LeadTransaction([
            'id_lead' => $this->getNewIdLead(),
            'include' => 'creater',
        ]);

        $response = $gateway->viewLead($data);

        $this->assertInstanceOf(Status::class, $response);
        $this->assertObjectHasAttribute('status', $response, Status::ST_OK);
        $this->assertObjectHasAttribute('reason', $response, ReasonCodes::APPROVED_TRANSACTION);
        $this->assertObjectHasAttribute('message', $response);
        $this->assertObjectHasAttribute('date', $response, date('c'));
    }

    public function testItCanDeleteALeadSuccessful(): void
    {
        $gateway = $this->gateway();

        $data = new LeadTransaction([
            'id_lead' => $this->getNewIdLead(),
        ]);

        $response = $gateway->deleteLead($data);

        $this->assertInstanceOf(Status::class, $response);
        $this->assertObjectHasAttribute('status', $response, Status::ST_OK);
        $this->assertObjectHasAttribute('reason', $response, ReasonCodes::APPROVED_TRANSACTION);
        $this->assertObjectHasAttribute('message', $response, true);
        $this->assertObjectHasAttribute('date', $response, date('c'));
    }

    public function testItCannotDeleteALeadWithoutTheIdLeadField(): void
    {
        $gateway = $this->gateway();

        $data = new LeadTransaction([
        ]);

        $this->expectException(FreshsalesSdkException::class);
        $this->expectExceptionMessage(ExceptionMessages::ID_LEAD);

        $response = $gateway->deleteLead($data);
    }

    public function testItCannotDeleteALeadFailed(): void
    {
        $gateway = $this->gateway();

        $response = $gateway->deleteLead(new LeadTransaction([
            'id_lead' => 1,
        ]));

        $this->assertInstanceOf(Status::class, $response);
        $this->assertObjectHasAttribute('status', $response, Status::ST_FAILED);
        $this->assertObjectHasAttribute('reason', $response, ReasonCodes::INVALID_RESPONSE);
        $this->assertObjectHasAttribute('message', $response);
        $this->assertObjectHasAttribute('date', $response, date('c'));
    }

    public function testItCanForgetALeadSuccessful(): void
    {
        $gateway = $this->gateway();

        $data = new LeadTransaction([
            'id_lead' => $this->getNewIdLead(),
        ]);

        $response = $gateway->forgetLead($data);

        $this->assertInstanceOf(Status::class, $response);
        $this->assertObjectHasAttribute('status', $response, Status::ST_OK);
        $this->assertObjectHasAttribute('reason', $response, ReasonCodes::APPROVED_TRANSACTION);
        $this->assertObjectHasAttribute('message', $response);
        $this->assertObjectHasAttribute('date', $response, date('c'));
    }

    public function testItCannotForgetALeadWithoutTheIdLeadField(): void
    {
        $gateway = $this->gateway();

        $data = new LeadTransaction([
        ]);

        $this->expectException(FreshsalesSdkException::class);
        $this->expectExceptionMessage(ExceptionMessages::ID_LEAD);

        $response = $gateway->forgetLead($data);
    }

    public function testItCannotForgetALeadFailed(): void
    {
        $gateway = $this->gateway();

        $response = $gateway->forgetLead(new LeadTransaction([
            'id_lead' => 1,
        ]));

        $this->assertInstanceOf(Status::class, $response);
        $this->assertObjectHasAttribute('status', $response, Status::ST_FAILED);
        $this->assertObjectHasAttribute('reason', $response, ReasonCodes::INVALID_RESPONSE);
        $this->assertObjectHasAttribute('message', $response);
        $this->assertObjectHasAttribute('date', $response, date('c'));
    }

    public function testItCanBulkDeleteLeadsSuccessful(): void
    {
        $gateway = $this->gateway();

        $data = new LeadTransaction([
            'selected_ids' => [$this->getNewIdLead(), $this->getNewIdLead()],
        ]);

        $response = $gateway->bulkDeleteLeads($data);

        $this->assertInstanceOf(Status::class, $response);
        $this->assertObjectHasAttribute('status', $response, Status::ST_OK);
        $this->assertObjectHasAttribute('reason', $response, ReasonCodes::APPROVED_TRANSACTION);
        $this->assertObjectHasAttribute('message', $response);
        $this->assertObjectHasAttribute('date', $response, date('c'));
    }

    public function testItCannotBulkDeleteLeadsWithoutTheSelectedIdsField(): void
    {
        $gateway = $this->gateway();

        $data = new LeadTransaction([
            'selected_ids' => [],
        ]);

        $this->expectException(FreshsalesSdkException::class);
        $this->expectExceptionMessage(ExceptionMessages::SELECTED_IDS);

        $response = $gateway->bulkDeleteLeads($data);
    }

    public function testItCanUpdateALeadSuccessful(): void
    {
        $gateway = $this->gateway();

        $response = $gateway->updateLead(new LeadTransaction([
            'first_name' => 'Juan',
            'last_name' => 'Pabon',
            'email' => 'juan.pabon@evertecinc.com',
            'mobile_number' => '3006719234',
            'company' => ['name' => 'Evertec Inc'],
            'job_title' => 'TI',
            'medium' => 'Portal autogestión',
            'id_lead' => $this->getNewIdLead(),
        ]));

        $this->assertInstanceOf(Status::class, $response);
        $this->assertObjectHasAttribute('status', $response, Status::ST_OK);
        $this->assertObjectHasAttribute('reason', $response, ReasonCodes::APPROVED_TRANSACTION);
        $this->assertObjectHasAttribute('message', $response);
        $this->assertObjectHasAttribute('date', $response, date('c'));
    }

    public function testItCannotUpdateALeadWithoutTheIdLeadField(): void
    {
        $gateway = $this->gateway();

        $this->expectException(FreshsalesSdkException::class);
        $this->expectExceptionMessage(ExceptionMessages::ID_LEAD);

        $response = $gateway->updateLead(new LeadTransaction([
            'first_name' => 'Juan',
            'last_name' => 'Pabon',
            'email' => 'juan.pabon@evertecinc.com',
            'mobile_number' => '3006719234',
            'company' => ['name' => 'Evertec Inc'],
            'job_title' => 'TI',
            'medium' => 'Portal autogestión',
        ]));
    }

    public function testItCannotUpdateALeadFailed(): void
    {
        $gateway = $this->gateway();

        $response = $gateway->updateLead(new LeadTransaction([
            'id_lead' => 1,
        ]));

        $this->assertInstanceOf(Status::class, $response);
        $this->assertObjectHasAttribute('status', $response, Status::ST_FAILED);
        $this->assertObjectHasAttribute('reason', $response, ReasonCodes::INVALID_RESPONSE);
        $this->assertObjectHasAttribute('message', $response);
        $this->assertObjectHasAttribute('date', $response, date('c'));
    }

    /*
    public function testItCanConvertALeadSuccessful(): void
    {
        $gateway = $this->gateway();

        $response = $gateway->convertLead(new LeadTransaction([
            'first_name' => 'Juan',
            'last_name' => 'Pabon',
            'email' => 'juan.pabon@evertecinc.com',
            'mobile_number' => '3006719234',
            'company' => ['name' => 'Evertec Inc'],
            'job_title' => 'TI',
            'medium' => 'Portal autogestión',
            'id_lead' => $this->getNewIdLead(),
        ]));

        dd($response);

        $this->assertInstanceOf(Status::class, $response);
        $this->assertObjectHasAttribute('status', $response, Status::ST_OK);
        $this->assertObjectHasAttribute('reason', $response, ReasonCodes::APPROVED_TRANSACTION);
        $this->assertObjectHasAttribute('message', $response);
        $this->assertObjectHasAttribute('date', $response, date('c'));
    } */

    public function testItCannotConvertALeadWithoutTheIdLeadField(): void
    {
        $gateway = $this->gateway();

        $this->expectException(FreshsalesSdkException::class);
        $this->expectExceptionMessage(ExceptionMessages::ID_LEAD);

        $response = $gateway->convertLead(new LeadTransaction([
            'first_name' => 'Juan',
            'last_name' => 'Pabon',
            'email' => 'juan.pabon@evertecinc.com',
            'mobile_number' => '3006719234',
            'company' => ['name' => 'Evertec Inc'],
            'job_title' => 'TI',
            'medium' => 'Portal autogestión',
        ]));
    }

    public function testItCannotConvertALeadFailed(): void
    {
        $gateway = $this->gateway();

        $response = $gateway->convertLead(new LeadTransaction([
            'id_lead' => 1,
        ]));

        $this->assertInstanceOf(Status::class, $response);
        $this->assertObjectHasAttribute('status', $response, Status::ST_FAILED);
        $this->assertObjectHasAttribute('reason', $response, ReasonCodes::INVALID_RESPONSE);
        $this->assertObjectHasAttribute('message', $response);
        $this->assertObjectHasAttribute('date', $response, date('c'));
    }

    public function testItCanCloneALeadSuccessful(): void
    {
        $gateway = $this->gateway();

        $data = new LeadTransaction([
            'id_lead' => $this->getNewIdLead(),
        ]);

        $response = $gateway->cloneLead($data);

        $this->assertInstanceOf(Status::class, $response);
        $this->assertObjectHasAttribute('status', $response, Status::ST_OK);
        $this->assertObjectHasAttribute('reason', $response, ReasonCodes::APPROVED_TRANSACTION);
        $this->assertObjectHasAttribute('message', $response);
        $this->assertObjectHasAttribute('date', $response, date('c'));
    }

    public function testItCannotCloneALeadFailed(): void
    {
        $gateway = $this->gateway();

        $response = $gateway->cloneLead(new LeadTransaction([
            'id_lead' => 1,
        ]));

        $this->assertInstanceOf(Status::class, $response);
        $this->assertObjectHasAttribute('status', $response, Status::ST_FAILED);
        $this->assertObjectHasAttribute('reason', $response, ReasonCodes::INVALID_RESPONSE);
        $this->assertObjectHasAttribute('message', $response);
        $this->assertObjectHasAttribute('date', $response, date('c'));
    }

    public function testItCannotCloneALeadWithoutTheIdLeadField(): void
    {
        $gateway = $this->gateway();

        $data = new LeadTransaction([
        ]);

        $this->expectException(FreshsalesSdkException::class);
        $this->expectExceptionMessage(ExceptionMessages::ID_LEAD);

        $response = $gateway->cloneLead($data);
    }

    public function testItCannotListAllLeadsWithoutAuthorization(): void
    {
        $gateway = $this->gateway();

        $data = new LeadTransaction([
            'id_view' => 4,
        ]);

        $response = $gateway->listAllLeads($data);

        $this->assertInstanceOf(Status::class, $response);
        $this->assertObjectHasAttribute('status', $response, Status::ST_FAILED);
        $this->assertObjectHasAttribute('reason', $response, ReasonCodes::INVALID_RESPONSE);
        $this->assertObjectHasAttribute('message', $response);
        $this->assertObjectHasAttribute('date', $response, date('c'));
    }

    public function testItCannotListAllLeadsWithoutTheIdViewField(): void
    {
        $gateway = $this->gateway();

        $data = new LeadTransaction([
        ]);

        $this->expectException(FreshsalesSdkException::class);
        $this->expectExceptionMessage(ExceptionMessages::ID_VIEW);

        $response = $gateway->listAllLeads($data);
    }

    public function testItCanListAllLeadFieldsSuccessful(): void
    {
        $gateway = $this->gateway();

        $data = new LeadTransaction([
        ]);

        $response = $gateway->listAllLeadFields($data);

        $this->assertInstanceOf(Status::class, $response);
        $this->assertObjectHasAttribute('status', $response, Status::ST_OK);
        $this->assertObjectHasAttribute('reason', $response, ReasonCodes::APPROVED_TRANSACTION);
        $this->assertObjectHasAttribute('message', $response);
        $this->assertObjectHasAttribute('date', $response, date('c'));
    }

    public function testItCanListAllLeadFieldsWithTheInclude(): void
    {
        $gateway = $this->gateway();

        $data = new LeadTransaction([
            'include' => 'field_group',
        ]);

        $response = $gateway->listAllLeadFields($data);

        $this->assertInstanceOf(Status::class, $response);
        $this->assertObjectHasAttribute('status', $response, Status::ST_OK);
        $this->assertObjectHasAttribute('reason', $response, ReasonCodes::APPROVED_TRANSACTION);
        $this->assertObjectHasAttribute('message', $response);
        $this->assertObjectHasAttribute('date', $response, date('c'));
    }

    public function testItCanListAllActivitiesSuccessful(): void
    {
        $gateway = $this->gateway();

        $data = new LeadTransaction([
            'id_lead' => $this->getNewIdLead(),
        ]);

        $response = $gateway->listAllActivities($data);

        $this->assertInstanceOf(Status::class, $response);
        $this->assertObjectHasAttribute('status', $response, Status::ST_OK);
        $this->assertObjectHasAttribute('reason', $response, ReasonCodes::APPROVED_TRANSACTION);
        $this->assertObjectHasAttribute('message', $response);
        $this->assertObjectHasAttribute('date', $response, date('c'));
    }

    public function testItCannotListAllActivitiesWithoutTheIdLeadField(): void
    {
        $gateway = $this->gateway();

        $data = new LeadTransaction([
        ]);

        $this->expectException(FreshsalesSdkException::class);
        $this->expectExceptionMessage(ExceptionMessages::ID_LEAD);

        $response = $gateway->listAllActivities($data);
    }

    public function testItCannotListAllActivitiesFailed(): void
    {
        $gateway = $this->gateway();

        $response = $gateway->listAllActivities(new LeadTransaction([
            'id_lead' => 1,
        ]));

        $this->assertInstanceOf(Status::class, $response);
        $this->assertObjectHasAttribute('status', $response, Status::ST_FAILED);
        $this->assertObjectHasAttribute('reason', $response, ReasonCodes::INVALID_RESPONSE);
        $this->assertObjectHasAttribute('message', $response);
        $this->assertObjectHasAttribute('date', $response, date('c'));
    }

    public function testItCanDoLogOfCreateALead(): void
    {
        $logger = new TestLogger;

        $gateway = $this->gatewayWithLog($logger);

        $response = $gateway->createLead($this->dataRequest());

        $this->assertInstanceOf(TestLogger::class, $logger);
        $this->assertObjectHasAttribute('records', $logger);
        $this->assertObjectHasAttribute('recordsByLevel', $logger);
    }

    public function testItCanDoLogOfViewALead(): void
    {
        $logger = new TestLogger;

        $gateway = $this->gatewayWithLog($logger);

        $data = new LeadTransaction([
            'id_lead' => $this->getNewIdLead(),
        ]);

        $response = $gateway->viewLead($data);

        $this->assertInstanceOf(TestLogger::class, $logger);
        $this->assertObjectHasAttribute('records', $logger);
        $this->assertObjectHasAttribute('recordsByLevel', $logger);
    }

    public function testItCanDoLogOfDeleteALead(): void
    {
        $logger = new TestLogger;

        $gateway = $this->gatewayWithLog($logger);

        $data = new LeadTransaction([
            'id_lead' => $this->getNewIdLead(),
        ]);

        $response = $gateway->deleteLead($data);

        $this->assertInstanceOf(TestLogger::class, $logger);
        $this->assertObjectHasAttribute('records', $logger);
        $this->assertObjectHasAttribute('recordsByLevel', $logger);
    }

    public function testItCanDoLogOfForgetALead(): void
    {
        $logger = new TestLogger;

        $gateway = $this->gatewayWithLog($logger);

        $data = new LeadTransaction([
            'id_lead' => $this->getNewIdLead(),
        ]);

        $response = $gateway->forgetLead($data);

        $this->assertInstanceOf(TestLogger::class, $logger);
        $this->assertObjectHasAttribute('records', $logger);
        $this->assertObjectHasAttribute('recordsByLevel', $logger);
    }

    public function testItCanDoLogOfBulkDeleteLeads(): void
    {
        $logger = new TestLogger;

        $gateway = $this->gatewayWithLog($logger);

        $data = new LeadTransaction([
            'selected_ids' => [$this->getNewIdLead(), $this->getNewIdLead()],
        ]);

        $response = $gateway->bulkDeleteLeads($data);

        $this->assertInstanceOf(TestLogger::class, $logger);
        $this->assertObjectHasAttribute('records', $logger);
        $this->assertObjectHasAttribute('recordsByLevel', $logger);
    }

    public function testItCanDoLogOfUpdateALead(): void
    {
        $logger = new TestLogger;

        $gateway = $this->gatewayWithLog($logger);

        $data = new LeadTransaction([
            'selected_ids' => [$this->getNewIdLead(), $this->getNewIdLead()],
        ]);

        $response = $gateway->updateLead(new LeadTransaction([
            'first_name' => 'Juan',
            'last_name' => 'Pabon',
            'email' => 'juan.pabon@evertecinc.com',
            'mobile_number' => '3006719234',
            'company' => ['name' => 'Evertec Inc'],
            'job_title' => 'TI',
            'medium' => 'Portal autogestión',
            'id_lead' => '8019988769',
        ]));

        $this->assertInstanceOf(TestLogger::class, $logger);
        $this->assertObjectHasAttribute('records', $logger);
        $this->assertObjectHasAttribute('recordsByLevel', $logger);
    }

    public function testItCanDoLogOfConvertALead(): void
    {
        $logger = new TestLogger;

        $gateway = $this->gatewayWithLog($logger);

        $response = $gateway->convertLead(new LeadTransaction([
            'id_lead' => 1,
        ]));

        $this->assertInstanceOf(TestLogger::class, $logger);
        $this->assertObjectHasAttribute('records', $logger);
        $this->assertObjectHasAttribute('recordsByLevel', $logger);
    }

    public function testItCanDoLogOfCloneALead(): void
    {
        $logger = new TestLogger;

        $gateway = $this->gatewayWithLog($logger);

        $response = $gateway->cloneLead(new LeadTransaction([
            'id_lead' => 1,
        ]));

        $this->assertInstanceOf(TestLogger::class, $logger);
        $this->assertObjectHasAttribute('records', $logger);
        $this->assertObjectHasAttribute('recordsByLevel', $logger);
    }

    public function testItCanDoLogOfListAllLeads(): void
    {
        $logger = new TestLogger;

        $gateway = $this->gatewayWithLog($logger);

        $data = new LeadTransaction([
            'id_view' => 4,
        ]);

        $response = $gateway->listAllLeads($data);

        $this->assertInstanceOf(TestLogger::class, $logger);
        $this->assertObjectHasAttribute('records', $logger);
        $this->assertObjectHasAttribute('recordsByLevel', $logger);
    }

    public function testItCanDoLogOfListAllLeadFields(): void
    {
        $logger = new TestLogger;

        $gateway = $this->gatewayWithLog($logger);

        $data = new LeadTransaction([
        ]);

        $response = $gateway->listAllLeadFields($data);

        $this->assertInstanceOf(TestLogger::class, $logger);
        $this->assertObjectHasAttribute('records', $logger);
        $this->assertObjectHasAttribute('recordsByLevel', $logger);
    }

    public function testItCanDoLogOfListAllActivities(): void
    {
        $logger = new TestLogger;

        $gateway = $this->gatewayWithLog($logger);

        $data = new LeadTransaction([
            'id_lead' => $this->getNewIdLead(),
        ]);

        $response = $gateway->listAllActivities($data);

        $this->assertInstanceOf(TestLogger::class, $logger);
        $this->assertObjectHasAttribute('records', $logger);
        $this->assertObjectHasAttribute('recordsByLevel', $logger);
    }
}
