<?php

namespace Tests\Unit;

use Pabon\FreshsalesSdk\Entities\LeadTransaction;
use Pabon\FreshsalesSdk\Gateway;
use Tests\TestCase;

class FreshsalesTransactionTest extends TestCase
{
    private function dataRequest(): array
    {
        return [
            'full_name' => 'juan doe',
            'email' => 'juan.doe@test.com',
            'mobile_number' => '3006719234',
            'company' => ['name' => 'Evertec Inc'],
            'job_title' => 'TI',
            'medium' => 'Portal autogestión',
            'id_lead' => '8019988769',
            'emails' => [],
            'work_number' => '3053347566',
            'address' => 'Cll 45 #50',
            'city' => 'Medellin',
            'state' => 'Antioquia',
            'zipcode' => '04',
            'subscription_status' => ['Correct'],
            'country' => 'Colombia',
            'lead_stage_id' => 2,
            'lead_reason_id' => 2,
            'lead_source_id' => 2,
            'owner_id' => 23454321,
            'campaign_id' => 1,
            'keyword' => 'example',
            'time_zone' => 'UTF USD',
            'facebook' => 'JuanPa...',
            'twitter' => '@juan.pa',
            'linkedin' => 'no se',
            'territory_id' => '3',
            'deal' => ['example'],
            'created_at' => '24-01-12',
            'updated_at' => '24-01-22',
            'additional' => ['hi' => 'text'],
            'include' => 'creater',
            'selected_ids' => [12345, 98765],
            'id_view' => 1,
        ];
    }

    public function testItCanCorrectlyAnalyzeTheCreationOfALeadTransaction(): void
    {
        $request = new LeadTransaction($this->dataRequest());

        $this->assertEquals($this->dataRequest()['full_name'], $request->getFullName());
        $this->assertEquals($this->dataRequest()['email'], $request->getEmail());
        $this->assertEquals($this->dataRequest()['mobile_number'], $request->getMobileNumber());
        $this->assertEquals($this->dataRequest()['company'], $request->getCompany());
        $this->assertEquals($this->dataRequest()['job_title'], $request->getJobTitle());
        $this->assertEquals($this->dataRequest()['medium'], $request->getMedium());
        $this->assertEquals($this->dataRequest()['id_lead'], $request->getIdLead());
        $this->assertEquals($this->dataRequest()['subscription_status'], $request->getSubscriptionStatus());
        $this->assertEquals($this->dataRequest()['emails'], $request->getEmails());
        $this->assertEquals($this->dataRequest()['work_number'], $request->getWorkNumber());
        $this->assertEquals($this->dataRequest()['address'], $request->getAddress());
        $this->assertEquals($this->dataRequest()['city'], $request->getCity());
        $this->assertEquals($this->dataRequest()['state'], $request->getState());
        $this->assertEquals($this->dataRequest()['zipcode'], $request->getZipcode());
        $this->assertEquals($this->dataRequest()['country'], $request->getCountry());
        $this->assertEquals($this->dataRequest()['lead_stage_id'], $request->getLeadStageId());
        $this->assertEquals($this->dataRequest()['lead_reason_id'], $request->getLeadReasonId());
        $this->assertEquals($this->dataRequest()['lead_source_id'], $request->getLeadSourceId());
        $this->assertEquals($this->dataRequest()['owner_id'], $request->getOwnerId());
        $this->assertEquals($this->dataRequest()['campaign_id'], $request->getCampaignId());
        $this->assertEquals($this->dataRequest()['keyword'], $request->getKeyword());
        $this->assertEquals($this->dataRequest()['time_zone'], $request->getTimeZone());
        $this->assertEquals($this->dataRequest()['facebook'], $request->getFacebook());
        $this->assertEquals($this->dataRequest()['twitter'], $request->getTwitter());
        $this->assertEquals($this->dataRequest()['linkedin'], $request->getLinkedin());
        $this->assertEquals($this->dataRequest()['territory_id'], $request->getTerritoryId());
        $this->assertEquals($this->dataRequest()['deal'], $request->getDeal());
        $this->assertEquals($this->dataRequest()['created_at'], $request->getCreatedAt());
        $this->assertEquals($this->dataRequest()['updated_at'], $request->getUpdatedAt());
        $this->assertEquals($this->dataRequest()['additional'], $request->additional());
        $this->assertEquals($this->dataRequest()['include'], $request->getInclude());
        $this->assertEquals($this->dataRequest()['selected_ids'], $request->getSelectedIds());
        $this->assertEquals($this->dataRequest()['id_view'], $request->getIdView());
    }
}
