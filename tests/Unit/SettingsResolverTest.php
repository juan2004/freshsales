<?php

namespace Tests\Unit;

use Pabon\FreshsalesSdk\Simulators\ClientSimulator;
use Pabon\FreshsalesSdk\Support\SettingsResolver;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PlacetoPay\Tangram\Entities\Cache;
use PlacetoPay\Tangram\Mock\TestLogger;
use Symfony\Component\OptionsResolver\Exception\InvalidOptionsException;
use Symfony\Component\OptionsResolver\Exception\MissingOptionsException;

class SettingsResolverTest extends TestCase
{
    private array $data;

    private function getdata(): void
    {
        $this->data['url'] = 'https://dev.placetopay.com';
        $this->data['token'] = 'string';
    }

    protected function validLoggerSettings(): array
    {
        return [
            'name' => 'valid name',
            'via' => new TestLogger(),
            'path' => 'valid/path',
        ];
    }

    public function testTheTokenIsMandatory(): void
    {
        $this->expectException(MissingOptionsException::class);
        $this->expectExceptionMessage('The required option "token" is missing.');

        $this->data['url'] = 'https://dev.placetopay.com';

        $resolver = SettingsResolver::create($this->data);
        $resolver->resolve($this->data);
    }

    public function testItValidatesTokenIsAString(): void
    {
        $this->expectException(InvalidOptionsException::class);
        $this->expectExceptionMessage('The option "token" with value array is expected to be of type "string", but is of type "array".');

        $this->data['token'] = ['Array'];
        $this->data['url'] = 'https://dev.placetopay.com';

        $resolver = SettingsResolver::create($this->data);
        $resolver->resolve($this->data);
    }

    public function testTheUrlIsMandatory(): void
    {
        $this->expectException(MissingOptionsException::class);
        $this->expectExceptionMessage('The required option "url" is missing.');

        $this->data['token'] = 'string';

        $resolver = SettingsResolver::create($this->data);
        $resolver->resolve($this->data);
    }

    public function testItValidatesUrlIsAString(): void
    {
        $this->expectException(InvalidOptionsException::class);
        $this->expectExceptionMessage('The option "url" with value array is expected to be of type "string", but is of type "array".');

        $this->data['url'] = ['Array'];
        $this->data['token'] = 'string';

        $resolver = SettingsResolver::create($this->data);
        $resolver->resolve($this->data);
    }

    public function testItDefinesADefaultClient(): void
    {
        $this->getdata();

        $resolver = SettingsResolver::create($this->data);

        $this->assertInstanceOf(Client::class, $resolver->resolve($this->data)['client']);
    }

    public function testItDefinesADefaultClientSimulatorWhenInSimulatorMode(): void
    {
        $this->getdata();
        $this->data['simulatorMode'] = true;

        $resolver = SettingsResolver::create($this->data);

        $this->assertInstanceOf(ClientSimulator::class, $resolver->resolve($this->data)['client']);
    }

    public function testItValidatesClientImplementsClientInterface(): void
    {
        $this->expectException(InvalidOptionsException::class);
        $this->expectExceptionMessage('The option "client" with value "new client" is expected to be of type "GuzzleHttp\ClientInterface", but is of type "string".');

        $this->getdata();
        $this->data['client'] = 'new client';

        $resolver = SettingsResolver::create($this->data);
        $resolver->resolve($this->data);
    }

    public function testItDefinesADefaultCache(): void
    {
        $this->getdata();
        $resolver = SettingsResolver::create($this->data);

        $this->assertInstanceOf(Cache::class, $resolver->resolve($this->data)['cache']);
    }

    public function testItValidatesClientImplementsCacheInterface(): void
    {
        $this->expectException(InvalidOptionsException::class);
        $this->expectExceptionMessage('The option "cache" with value "new cache" is expected to be of type "Psr\SimpleCache\CacheInterface", but is of type "string".');

        $this->data['cache'] = 'new cache';

        $this->getdata();
        $resolver = SettingsResolver::create($this->data);
        $resolver->resolve($this->data);
    }

    public function testItDefinesSimulatorModeByDefaultAsFalse(): void
    {
        $this->getdata();
        $resolver = SettingsResolver::create($this->data);

        $this->assertFalse($resolver->resolve($this->data)['simulatorMode']);
    }

    public function testItValidatesSimulatorModeIsBoolean(): void
    {
        $this->expectException(InvalidOptionsException::class);
        $this->expectExceptionMessage('The option "simulatorMode" with value "falsy" is expected to be of type "bool", but is of type "string".');

        $this->getdata();
        $this->data['simulatorMode'] = 'falsy';

        $resolver = SettingsResolver::create($this->data);
        $resolver->resolve($this->data);
    }

    public function testItValidatesLoggerIsAnArray(): void
    {
        $this->expectException(InvalidOptionsException::class);
        $this->expectExceptionMessage('The nested option "logger" with value "logger" is expected to be of type array, but is of type "string".');

        $this->getdata();
        $this->data['logger'] = 'logger';

        $resolver = SettingsResolver::create($this->data);
        $resolver->resolve($this->data);
    }

    public function testItValidatesLoggerNameIsAString(): void
    {
        $this->expectException(InvalidOptionsException::class);
        $this->expectExceptionMessage('The option "logger[name]" with value array is expected to be of type "string", but is of type "array".');

        $this->getdata();
        $this->data['logger'] = array_replace($this->validLoggerSettings(), [
            'name' => ['array name'],
        ]);

        $resolver = SettingsResolver::create($this->data);
        $resolver->resolve($this->data);
    }

    public function testItValidatesLoggerViaIsRequired(): void
    {
        $this->expectException(MissingOptionsException::class);
        $this->expectExceptionMessage('The required option "logger[via]" is missing.');

        $loggerSetting = $this->validLoggerSettings();
        unset($loggerSetting['via']);

        $this->getdata();
        $this->data['logger'] = $loggerSetting;

        $resolver = SettingsResolver::create($this->data);
        $resolver->resolve($this->data);
    }

    public function testItValidatesLoggerViaImplementsLoggerInterface(): void
    {
        $this->expectException(InvalidOptionsException::class);
        $this->expectExceptionMessage('The option "logger[via]" with value "string via" is expected to be of type "Psr\Log\LoggerInterface", but is of type "string".');

        $this->getdata();
        $this->data['logger'] = array_replace($this->validLoggerSettings(), [
            'via' => 'string via',
        ]);

        $resolver = SettingsResolver::create($this->data);
        $resolver->resolve($this->data);
    }

    public function testItValidatesLoggerPathIsAString(): void
    {
        $this->expectException(InvalidOptionsException::class);
        $this->expectExceptionMessage('The option "logger[path]" with value array is expected to be of type "string" or "null", but is of type "array".');

        $this->getdata();
        $this->data['logger'] = array_replace($this->validLoggerSettings(), [
            'path' => ['array path'],
        ]);

        $resolver = SettingsResolver::create($this->data);
        $resolver->resolve($this->data);
    }

    public function testItDefinesLoggerDebugByDefaultAsFalse(): void
    {
        $this->getdata();
        $this->data['logger'] = $this->validLoggerSettings();

        $resolver = SettingsResolver::create($this->data);

        $this->assertFalse($resolver->resolve($this->data)['logger']['debug']);
    }

    public function testItValidatesLoggerDebugIsABoolean(): void
    {
        $this->expectException(InvalidOptionsException::class);
        $this->expectExceptionMessage('The option "logger[debug]" with value "falsy" is expected to be of type "bool", but is of type "string".');

        $this->getdata();
        $this->data['logger'] = array_replace($this->validLoggerSettings(), [
            'debug' => 'falsy',
        ]);

        $resolver = SettingsResolver::create($this->data);
        $resolver->resolve($this->data);
    }
}
