# Freshsales Sdk

Software development kit to connect with the Freshsale's APIs

## Installation
You should add PlacetoPay repository:
```json
{
    "repositories": [
        {
            "type": "composer",
            "url": "https://dev.placetopay.com/repository"
        }
    ]
}
```

Then, you can install the package via composer:
```
composer require placetopay/freshsales-sdk
```

## Usage

The first thing to do is create an instance of the Gateway class and pass the credentials (token, url) 
in the parameter as an array. Example:
```
return new Gateway([
    'token' => 'mdwkdwkdwkwnk123',
    'url' => 'https://placetopay...',
]);
```

## Available methods

### createLead():
To create a lead, you must call the createLead() method from the Gateway class instance and pass as a parameter an 
instance of the LeadTransaction class that receives an array with the request fields. Example:
```
$response = $gateway->createLead(new LeadTransaction([
    'full_name' => 'juan doe',
    'email' => 'juan.doe@test.com',
    'mobile_number' => '3006719234',
    'company' => ['name' => 'Evertec Inc'],
    'job_title' => 'TI',
    'medium' => 'Portal autogestión',
    'id_lead' => '8019988769',
    'emails' => [],
    'work_number' => '3053347566',
    'address' => 'Cll 45 #50',
    'city' => 'Medellin',
    'state' => 'Antioquia',
    'zipcode' => '04',
    'subscription_status' => ['Correct'],
    'country' => 'Colombia',
    'lead_stage_id' => 2,
    'lead_reason_id' => 2,
    'lead_source_id' => 2,
    'campaign_id' => 1,
    'keyword' => 'example',
    'time_zone' => 'UTF USD',
    'facebook' => 'JuanPa...',
    'twitter' => '@juan.pa',
    'linkedin' => 'no se',
    'territory_id' => '3',
    ])
);
```

### viewLead():
To view a lead, you must call the viewLead() method from the Gateway class instance and pass as a parameter an instance 
of the LeadTransaction class that receives an array with the 'id_lead' to view and optional 'include' to view additional
information. Example:
```
$data = new LeadTransaction([
    'id_lead' => 8019986178,
    'include' => 'creater',
]);

$response = $gateway->viewLead($data);
```

### deleteLead():
To delete a lead, you must call the deleteLead() method from the Gateway class instance and pass as a parameter an 
instance of the LeadTransaction class that receives an array with the 'id_lead' to delete. Example:
```
$data = new LeadTransaction([
    'id_lead' => 8019986178,
]);

$response = $gateway->deleteLead($data);
```

### forgetLead():
To forget a lead, you must call the forgetLead() method from the Gateway class instance and pass as a parameter an
instance of the LeadTransaction class that receives an array with the 'id_lead' to forget. Example:
```
$data = new LeadTransaction([
    'id_lead' => 8019986178,
]);

$response = $gateway->forgetLead($data);
```

### bulkDeleteLeads():
To bulk delete leads, you must call the bulkDeleteLeads() method from the Gateway class instance and pass as a parameter
an instance of the LeadTransaction class that receives an array with the 'selected_ids' to delete. Example:
```
$data = new LeadTransaction([
    'selected_ids' => [8020036255, 8020036256],
]);

$response = $gateway->bulkDeleteLeads($data);
```

### cloneLead():
To clone a lead, you must call the cloneLead() method from the Gateway class instance and pass as a parameter an
instance of the LeadTransaction class that receives an array with the 'id_lead' to clone. Example:
```
$data = new LeadTransaction([
    'id_lead' => 8019986178,
]);

$response = $gateway->cloneLead($data);
```

### updateLead():
To update a lead, you must call the updateLead() method from the Gateway class instance and pass as a parameter an
instance of the LeadTransaction class that receives an array with the request fields and the 'id_lead' to update. 
Example:
```
$response = $gateway->updateLead(new LeadTransaction([
    'id_lead' => '8019988769',
    'full_name' => 'juan doe',
    'email' => 'juan.doe@test.com',
    'mobile_number' => '3006719234',
    'company' => ['name' => 'Evertec Inc'],
    'job_title' => 'TI',
    'medium' => 'Portal autogestión',
    'emails' => [],
    'work_number' => '3053347566',
    'address' => 'Cll 45 #50',
    'city' => 'Medellin',
    'state' => 'Antioquia',
    'zipcode' => '04',
    'subscription_status' => ['Correct'],
    'country' => 'Colombia',
    'lead_stage_id' => 2,
    'lead_reason_id' => 2,
    'lead_source_id' => 2,
    'campaign_id' => 1,
    'keyword' => 'example',
    'time_zone' => 'UTF USD',
    'facebook' => 'JuanPa...',
    'twitter' => '@juan.pa',
    'linkedin' => 'no se',
    'territory_id' => '3',
    ])
);
```

### convertLead():
Important: You must also have authorization for this action.

To convert a lead, you must call the convertLead() method from the Gateway class instance and pass as a parameter an
instance of the LeadTransaction class that receives an array with the request fields and the 'id_lead' to convert. 
Example:
```
$response = $gateway->convertLead(new LeadTransaction([
    'id_lead' => '8019988769',
    'first_name' => 'Juan',
    'last_name' => 'Pabon',
    'email' => 'juan.pabon@evertecinc.com',
    'mobile_number' => '3006719234',
    'company' => ['name' => 'Evertec Inc'],
    'job_title' => 'TI',
    'medium' => 'Portal autogestión',
]));
```

### listAllLeads():
Important: You must also have authorization for this action.

To list all leads, you must call the listAllLeads() method from the Gateway class instance and pass as a parameter an
instance of the LeadTransaction class that receives an array with the 'id_view' to clone. Example:
```
$data = new LeadTransaction([
    'id_view' => 4,
]);

$response = $gateway->listAllLeads($data);
```

### listAllLeadFields():
Important: At the moment it is mandatory to pass the LeadTransaction instance as a parameter, even if it is without 
defined values.

To list all lead fields, you must call the listAllLeadFields() method from the Gateway class instance and pass as a parameter an
instance of the LeadTransaction class that receives an optional array with 'include' to view additional
information. Example:
```
$data = new LeadTransaction([
    'include' => 'field_group',
]);

$response = $gateway->listAllLeadFields($data);
```

### listAllActivities():
To list all activities, you must call the listAllActivities() method from the Gateway class instance and pass as a 
parameter an instance of the LeadTransaction class that receives an array with the 'id_lead' to list all activities. 
Example:
```
$data = new LeadTransaction([
    'id_lead' => 8020040473,
]);

$response = $gateway->listAllActivities($data);
```

## Responses
The response obtained in each of the available methods always belongs to a 'Status' type entity that is understood by 
the projects that incorporate the Placetopay 'Base' package. Which consists of: status, reason, message and date.
```
{
  +status: "OK"
  +reason: "00"
  +message: "{"message":"0 clientes potenciales eliminados."}"
  +date: "2022-11-24T16:25:39+00:00"
}
```

To get only the status
```
$response->status
```

To get only the reason
```
$response->reason
```

To get only the message
```
$response->message
```

To get only the date
```
$response->date
```

#### To obtain some specific data from the message, you must decode the JSON of the message property and access the property of the data that is required. 
Example
```
json_decode($response->message)->lead->id   //To get the id of a created lead
```

## Important

* Some features require authorization to use, i.e. the account administrator must allow the account user to
  provided token can use them.


* Remember that available methods must be passed an instance of LeadTransaction as a parameter to avoid an exception or error.


* The first_name and last_name fields can be passed separately in the request data or define the full_name field,
  which takes the first word as first_name and the last word as last_name.dx

#### For more information about the Freshsale's APIs see the documentation [Here](https://developer.freshsales.io/api/#introduction)