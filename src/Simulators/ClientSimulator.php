<?php

namespace Pabon\FreshsalesSdk\Simulators;

use Pabon\FreshsalesSdk\Constants\Endpoints;
use Pabon\FreshsalesSdk\Simulators\Behaviours\AuthenticationBehaviour;
use Pabon\FreshsalesSdk\Simulators\Behaviours\BaseSimulatorBehaviour;
use GuzzleHttp\Psr7\Response;
use PlacetoPay\Tangram\Mock\Client\HttpClientMock;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class ClientSimulator extends HttpClientMock
{
    protected const HANDLERS = [
        Endpoints::AUTHENTICATION => AuthenticationBehaviour::class,
    ];

    protected function createResponse(RequestInterface $request, array $options): ResponseInterface
    {
        /** @var BaseSimulatorBehaviour $behaviour */
        $behaviour = self::HANDLERS[$request->getUri()->getPath()] ?? null;

        if (! $behaviour) {
            return new Response(404);
        }

        return $behaviour::create()->resolve($request);
    }
}
