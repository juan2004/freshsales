<?php

namespace Pabon\FreshsalesSdk\Parsers;

use PlacetoPay\Base\Constants\ReasonCodes;
use PlacetoPay\Base\Entities\Status;
use PlacetoPay\Base\Messages\Transaction;
use Pabon\FreshsalesSdk\Constants\Endpoints;
use Pabon\FreshsalesSdk\Constants\ExceptionMessages;
use Pabon\FreshsalesSdk\Constants\Fields;
use Pabon\FreshsalesSdk\Exceptions\FreshsalesSdkException;
use PlacetoPay\Tangram\Contracts\CarrierDataObjectContract;
use PlacetoPay\Tangram\Contracts\ParserHandlerContract;

class UpdateLeadParser implements ParserHandlerContract
{
    public function parserRequest(CarrierDataObjectContract $carrierDataObject): array
    {
        $carrierDataObject->setOptions(array_merge([
            'method' => 'PUT',
            'endpoint' => $this->endpoint($carrierDataObject->transaction()->getIdLead()),
        ]));

        return [
            'json' => [
                Fields::ID => $carrierDataObject->transaction()->getIdLead(),
                Fields::FIRST_NAME => $carrierDataObject->transaction()->getFirstName(),
                Fields::LAST_NAME => $carrierDataObject->transaction()->getLastName(),
                Fields::EMAIL => $carrierDataObject->transaction()->getEmail(),
                Fields::MOBILE_NUMBER => $carrierDataObject->transaction()->getMobileNumber(),
                Fields::COMPANY => $carrierDataObject->transaction()->getCompany(),
                Fields::JOB_TITLE => $carrierDataObject->transaction()->getJobTitle(),
                Fields::MEDIUM => $carrierDataObject->transaction()->getMedium(),
                Fields::SUBSCRIPTION_STATUS => $carrierDataObject->transaction()->getSubscriptionStatus(),
                Fields::EMAILS => $carrierDataObject->transaction()->getEmails(),
                Fields::WORK_NUMBER => $carrierDataObject->transaction()->getWorkNumber(),
                Fields::ADDRESS => $carrierDataObject->transaction()->getAddress(),
                Fields::CITY => $carrierDataObject->transaction()->getCity(),
                Fields::STATE => $carrierDataObject->transaction()->getState(),
                Fields::ZIPCODE => $carrierDataObject->transaction()->getZipcode(),
                Fields::COUNTRY => $carrierDataObject->transaction()->getCountry(),
                Fields::LEAD_STAGE_ID => $carrierDataObject->transaction()->getLeadStageId(),
                Fields::LEAD_REASON_ID => $carrierDataObject->transaction()->getLeadReasonId(),
                Fields::LEAD_SOURCE_ID => $carrierDataObject->transaction()->getLeadSourceId(),
                Fields::OWNER_ID => $carrierDataObject->transaction()->getOwnerId(),
                Fields::CAMPAIGN_ID => $carrierDataObject->transaction()->getCampaignId(),
                Fields::KEYWORD => $carrierDataObject->transaction()->getKeyword(),
                Fields::TIME_ZONE => $carrierDataObject->transaction()->getTimeZone(),
                Fields::FACEBOOK => $carrierDataObject->transaction()->getFacebook(),
                Fields::TWITTER => $carrierDataObject->transaction()->getTwitter(),
                Fields::LINKEDIN => $carrierDataObject->transaction()->getLinkedin(),
                Fields::TERRITORY_ID => $carrierDataObject->transaction()->getTerritoryId(),
                Fields::DEAL => $carrierDataObject->transaction()->getDeal(),
                Fields::CREATED_AT => $carrierDataObject->transaction()->getCreatedAt(),
                Fields::UPDATED_AT => $carrierDataObject->transaction()->getUpdatedAt(),
            ],
        ];
    }

    public function parserResponse(CarrierDataObjectContract $carrierDataObject): Transaction
    {
        $carrierDataObject->transaction()->setStatus(Status::quickOk(
            ReasonCodes::APPROVED_TRANSACTION,
            $carrierDataObject->response()->getBody()->getContents(),
        ));

        return $carrierDataObject->transaction();
    }

    public function errorHandler(CarrierDataObjectContract $carrierDataObject): Transaction
    {
        $carrierDataObject->transaction()->setStatus(Status::quickFailed(
            ReasonCodes::INVALID_RESPONSE,
            $carrierDataObject->error()->getMessage()
        ));

        return $carrierDataObject->transaction();
    }

    private function endpoint(?string $endpoint): string
    {
        if (empty($endpoint)) {
            throw FreshsalesSdkException::forDataNotProvided(ExceptionMessages::ID_LEAD);
        }

        return Endpoints::ENDPOINT_BASE.'/'.$endpoint;
    }
}
