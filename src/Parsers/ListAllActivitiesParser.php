<?php

namespace Pabon\FreshsalesSdk\Parsers;

use PlacetoPay\Base\Constants\ReasonCodes;
use PlacetoPay\Base\Entities\Status;
use PlacetoPay\Base\Messages\Transaction;
use Pabon\FreshsalesSdk\Constants\Endpoints;
use Pabon\FreshsalesSdk\Constants\ExceptionMessages;
use Pabon\FreshsalesSdk\Exceptions\FreshsalesSdkException;
use PlacetoPay\Tangram\Contracts\CarrierDataObjectContract;
use PlacetoPay\Tangram\Contracts\ParserHandlerContract;

class ListAllActivitiesParser implements ParserHandlerContract
{
    public function parserRequest(CarrierDataObjectContract $carrierDataObject): array
    {
        $carrierDataObject->setOptions(array_merge([
            'method' => 'GET',
            'endpoint' => $this->endpoint($carrierDataObject->transaction()->getIdLead()),
        ]));

        return [];
    }

    public function parserResponse(CarrierDataObjectContract $carrierDataObject): Transaction
    {
        $carrierDataObject->transaction()->setStatus(Status::quickOk(
            ReasonCodes::APPROVED_TRANSACTION,
            $carrierDataObject->response()->getBody()->getContents(),
        ));

        return $carrierDataObject->transaction();
    }

    public function errorHandler(CarrierDataObjectContract $carrierDataObject): Transaction
    {
        $carrierDataObject->transaction()->setStatus(Status::quickFailed(
            ReasonCodes::INVALID_RESPONSE,
            $carrierDataObject->error()->getMessage()
        ));

        return $carrierDataObject->transaction();
    }

    private function endpoint(?string $endpoint): string
    {
        if (empty($endpoint)) {
            throw FreshsalesSdkException::forDataNotProvided(ExceptionMessages::ID_LEAD);
        }

        return Endpoints::ENDPOINT_BASE.'/'.$endpoint.Endpoints::ENDPOINT_LIST_ALL_ACTIVITIES;
    }
}
