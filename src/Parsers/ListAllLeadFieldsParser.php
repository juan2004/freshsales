<?php

namespace Pabon\FreshsalesSdk\Parsers;

use PlacetoPay\Base\Constants\ReasonCodes;
use PlacetoPay\Base\Entities\Status;
use PlacetoPay\Base\Messages\Transaction;
use Pabon\FreshsalesSdk\Constants\Endpoints;
use PlacetoPay\Tangram\Contracts\CarrierDataObjectContract;
use PlacetoPay\Tangram\Contracts\ParserHandlerContract;

class ListAllLeadFieldsParser implements ParserHandlerContract
{
    public function parserRequest(CarrierDataObjectContract $carrierDataObject): array
    {
        $carrierDataObject->setOptions(array_merge([
            'method' => 'GET',
            'endpoint' => $this->endpoint(
                $carrierDataObject->transaction()->getInclude()
            ),
        ]));

        return [];
    }

    public function parserResponse(CarrierDataObjectContract $carrierDataObject): Transaction
    {
        $carrierDataObject->transaction()->setStatus(Status::quickOk(
            ReasonCodes::APPROVED_TRANSACTION,
            $carrierDataObject->response()->getBody()->getContents(),
        ));

        return $carrierDataObject->transaction();
    }

    public function errorHandler(CarrierDataObjectContract $carrierDataObject): Transaction
    {
        $carrierDataObject->transaction()->setStatus(Status::quickFailed(
            ReasonCodes::INVALID_RESPONSE,
            $carrierDataObject->error()->getMessage()
        ));

        return $carrierDataObject->transaction();
    }

    private function endpoint(?string $include = ''): string
    {
        if (isset($include)) {
            return Endpoints::ENDPOINT_LIST_ALL_LEAD_FIELDS.'/'.'?include='.$include;
        }

        return Endpoints::ENDPOINT_LIST_ALL_LEAD_FIELDS;
    }
}
