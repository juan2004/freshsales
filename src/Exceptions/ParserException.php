<?php

namespace Pabon\FreshsalesSdk\Exceptions;

class ParserException extends FreshsalesSdkException
{
    public static function invalidOperation(string $operation): self
    {
        return new self(sprintf('Operation %s is not supported', $operation));
    }
}
