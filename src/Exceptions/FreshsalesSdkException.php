<?php

namespace Pabon\FreshsalesSdk\Exceptions;

use Exception;

class FreshsalesSdkException extends Exception
{
    public static function forDataNotProvided(string $message = ''): self
    {
        return new self($message);
    }
}
