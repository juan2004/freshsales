<?php

namespace Pabon\FreshsalesSdk\Constants;

class Operations
{
    public const CREATE_LEAD = 'createLead';
    public const VIEW_LEAD = 'viewLead';
    public const DELETE_LEAD = 'deleteLead';
    public const FORGET_LEAD = 'forgetLead';
    public const BULK_DELETE_LEADS = 'bulkDeleteLeads';
    public const UPDATE_LEAD = 'updateLead';
    public const CONVERT_LEAD = 'convertLead';
    public const CLONE_LEAD = 'cloneLead';
    public const LIST_ALL_LEADS = 'listAllLeads';
    public const LIST_ALL_LEAD_FIELDS = 'listAllLeadFields';
    public const LIST_ALL_ACTIVITIES = 'listAllActivities';
}
