<?php

namespace Pabon\FreshsalesSdk\Constants;

class Endpoints
{
    public const AUTHENTICATION = '/auth';
    public const ENDPOINT_BASE = '/api/leads';
    public const ENDPOINT_FORGET = '/forget';
    public const ENDPOINT_BULK_DELETE_LEADS = '/bulk_destroy';
    public const ENDPOINT_CONVERT = '/convert';
    public const ENDPOINT_CLONE = '/clone';
    public const ENDPOINT_LIST_ALL_LEAD_FIELDS = 'api/settings/leads/fields';
    public const ENDPOINT_LIST_ALL_LEADS = '/view';
    public const ENDPOINT_LIST_ALL_ACTIVITIES = '/activities';
}
