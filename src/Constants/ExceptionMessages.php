<?php

namespace Pabon\FreshsalesSdk\Constants;

class ExceptionMessages
{
    public const SELECTED_IDS = 'The field selected_ids is mandatory and must be of type array';
    public const ID_LEAD = 'The field id_lead is mandatory and must be of type numeric';
    public const ID_VIEW = 'The field id_view is mandatory and must be of type numeric';
}
