<?php

namespace Pabon\FreshsalesSdk\Constants;

class Fields
{
    public const ID = 'id';
    public const FIRST_NAME = 'first_name';
    public const LAST_NAME = 'last_name';
    public const EMAIL = 'email';
    public const MOBILE_NUMBER = 'mobile_number';
    public const COMPANY = 'company';
    public const JOB_TITLE = 'job_title';
    public const MEDIUM = 'medium';
    public const SUBSCRIPTION_STATUS = 'subscription_status';
    public const EMAILS = 'emails';
    public const WORK_NUMBER = 'work_number';
    public const ADDRESS = 'address';
    public const CITY = 'city';
    public const STATE = 'state';
    public const ZIPCODE = 'zipcode';
    public const COUNTRY = 'country';
    public const LEAD_STAGE_ID = 'lead_stage_id';
    public const LEAD_REASON_ID = 'lead_reason_id';
    public const LEAD_SOURCE_ID = 'lead_source_id';
    public const OWNER_ID = 'owner_id';
    public const CAMPAIGN_ID = 'campaign_id';
    public const KEYWORD = 'keyword';
    public const TIME_ZONE = 'time_zone';
    public const FACEBOOK = 'facebook';
    public const TWITTER = 'twitter';
    public const LINKEDIN = 'linkedin';
    public const TERRITORY_ID = 'territory_id';
    public const DEAL = 'deal';
    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = 'updated_at';

    public const INCLUDE = 'include';

    public const SELECTED_IDS = 'selected_ids';
}
