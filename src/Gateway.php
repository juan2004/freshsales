<?php

namespace Pabon\FreshsalesSdk;

use PlacetoPay\Base\Entities\Status;
use Pabon\FreshsalesSdk\Constants\Operations;
use Pabon\FreshsalesSdk\Entities\LeadTransaction;
use Pabon\FreshsalesSdk\Exceptions\ParserException;
use Pabon\FreshsalesSdk\Support\ParserManager;
use Pabon\FreshsalesSdk\Entities\Settings;
use Pabon\FreshsalesSdk\Support\SettingsResolver;
use PlacetoPay\Tangram\Carriers\RestCarrier;
use PlacetoPay\Tangram\Events\Dispatcher;
use PlacetoPay\Tangram\Exceptions\InvalidSettingException;
use PlacetoPay\Tangram\Listeners\HttpLoggerListener;
use PlacetoPay\Tangram\Services\BaseGateway;

class Gateway extends BaseGateway
{
    protected Settings $settings;
    protected RestCarrier $carrier;
    protected ParserManager $parserManager;

    /**
     * @throws InvalidSettingException
     */
    public function __construct(array $settings)
    {
        $this->settings = new Settings($settings, SettingsResolver::create($settings));
        $this->carrier = new RestCarrier($this->settings->client());
        $this->parserManager = new ParserManager($this->settings);

        $this->addEventDispatcher();
    }

    public function createLead(LeadTransaction $transaction): Status
    {
        return $this->process(Operations::CREATE_LEAD, $transaction)->status();
    }

    public function viewLead(LeadTransaction $transaction): Status
    {
        return $this->process(Operations::VIEW_LEAD, $transaction)->status();
    }

    public function deleteLead(LeadTransaction $transaction): Status
    {
        return $this->process(Operations::DELETE_LEAD, $transaction)->status();
    }

    public function forgetLead(LeadTransaction $transaction): Status
    {
        return $this->process(Operations::FORGET_LEAD, $transaction)->status();
    }

    public function bulkDeleteLeads(LeadTransaction $transaction): Status
    {
        return $this->process(Operations::BULK_DELETE_LEADS, $transaction)->status();
    }

    public function updateLead(LeadTransaction $transaction): Status
    {
        return $this->process(Operations::UPDATE_LEAD, $transaction)->status();
    }

    public function convertLead(LeadTransaction $transaction): Status
    {
        return $this->process(Operations::CONVERT_LEAD, $transaction)->status();
    }

    public function cloneLead(LeadTransaction $transaction): Status
    {
        return $this->process(Operations::CLONE_LEAD, $transaction)->status();
    }

    public function listAllLeads(LeadTransaction $transaction): Status
    {
        return $this->process(Operations::LIST_ALL_LEADS, $transaction)->status();
    }

    public function listAllLeadFields(LeadTransaction $transaction): Status
    {
        return $this->process(Operations::LIST_ALL_LEAD_FIELDS, $transaction)->status();
    }

    public function listAllActivities(LeadTransaction $transaction): Status
    {
        return $this->process(Operations::LIST_ALL_ACTIVITIES, $transaction)->status();
    }

    private function process(string $operation, LeadTransaction $transaction): LeadTransaction
    {
        try {
            $parser = $this->parserManager->getParser($operation);
            $carrierDataObjet = $this->parseRequest($operation, $transaction, $parser);
            $this->carrier->request($carrierDataObjet);
            $this->parseResponse($carrierDataObjet, $parser);

            return $transaction;
        } catch (ParserException $e) {
            throw ParserException::invalidOperation($operation);
        }
    }

    public function settings(): Settings
    {
        return $this->settings;
    }

    /**
     * @throws InvalidSettingException
     */
    protected function addEventDispatcher(): void
    {
        // Add Events Required
        if ($loggerSettings = $this->settings->loggerSettings()) {
            $this->setLoggerContext($loggerSettings);

            $listener = new HttpLoggerListener(
                $loggerSettings,
                $this->settings->providerName(),
                $this->settings->simulatorMode()
            );

            $this->carrier->setDispatcher(Dispatcher::create($listener->getEventsMethodsToDispatcher()));
        }
    }

    protected function setLoggerContext(&$loggerSettings): void
    {
        // Add masking rules for logs
        $loggerSettings['context'] = [
            'request' => [],
        ];
    }
}
