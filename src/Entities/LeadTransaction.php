<?php

namespace Pabon\FreshsalesSdk\Entities;

use PlacetoPay\Base\Constants\ReasonCodes;
use PlacetoPay\Base\Entities\Status;
use PlacetoPay\Base\Messages\Transaction;
use PlacetoPay\Base\Traits\LoaderTrait;
use Pabon\FreshsalesSdk\Constants\ExceptionMessages;
use Pabon\FreshsalesSdk\Exceptions\FreshsalesSdkException;

class LeadTransaction extends Transaction
{
    use LoaderTrait;

    protected ?int $id_lead = null;
    protected ?string $full_name = null;
    protected ?string $first_name = null;
    protected string $last_name = '';
    protected ?string $email = null;
    protected ?string $mobile_number = null;
    protected ?array $company = null;
    protected ?string $job_title = null;
    protected ?string $medium = null;
    protected ?array $subscription_status = null;
    protected ?array $emails = null;
    protected ?string $work_number = null;
    protected ?string $address = null;
    protected ?string $city = null;
    protected ?string $state = null;
    protected ?string $zipcode = null;
    protected ?string $country = null;
    protected ?int $lead_stage_id = null;
    protected ?int $lead_reason_id = null;
    protected ?int $lead_source_id = null;
    protected ?int $owner_id = null;
    protected ?int $campaign_id = null;
    protected ?string $keyword = null;
    protected ?string $time_zone = null;
    protected ?string $facebook = null;
    protected ?string $twitter = null;
    protected ?string $linkedin = null;
    protected ?int $territory_id = null;
    protected ?array $deal = null;
    protected ?string $created_at = null;
    protected ?string $updated_at = null;

    protected ?array $additional = [];

    protected ?string $include = null;

    protected array $selected_ids;

    protected ?int $id_view = null;

    public function __construct(array $data)
    {
        $this->status = Status::quick(Status::ST_PENDING, ReasonCodes::PENDING_TRANSACTION);
        $this->id = 'system-'.uniqid();

        $this->load($data, ['full_name', 'first_name', 'last_name', 'email', 'mobile_number', 'company', 'job_title', 'medium',
            'id_lead', 'subscription_status', 'emails', 'work_number', 'address', 'city', 'state', 'zipcode', 'country',
            'lead_stage_id', 'lead_reason_id', 'lead_source_id', 'owner_id', 'campaign_id', 'keyword', 'time_zone',
            'facebook', 'twitter', 'linkedin', 'territory_id', 'deal', 'created_at', 'updated_at', 'additional', 'include',
            'selected_ids', 'id_view', ]);
    }

    public function additional(?string $key = null, $default = null)
    {
        if ($key) {
            return $this->additional[$key] ?? $default;
        }

        return $this->additional;
    }

    public function setAdditional(?array $additional): self
    {
        $this->additional = $additional;

        return $this;
    }

    public function mergeAdditional(array $additionalExtra): self
    {
        $this->additional = array_replace($this->additional, $additionalExtra);

        return $this;
    }

    public function getFullName(): ?string
    {
        return $this->full_name;
    }

    public function getFirstName(): ?string
    {
        if (isset($this->full_name) && empty($this->first_name)) {
            $fullName = $this->getFullName();

            for ($i = 0; $i < strlen($fullName); $i++) {
                if (substr($fullName, $i, 1) != ' ') {
                    $this->first_name .= substr($fullName, $i, 1);
                } else {
                    return $this->first_name;
                }
            }
        }

        return $this->first_name;
    }

    public function getLastName(): string
    {
        if (isset($this->full_name) && empty($this->last_name)) {
            $fullName = $this->getFullName();

            $k = 0;
            for ($i = strlen($fullName); $i > 0; $i--) {
                if (substr($fullName, -$k, 1) != ' ') {
                    $this->last_name .= substr($fullName, $i, 1);
                    $k++;
                } else {
                    return strrev($this->last_name);
                }
            }
        }

        return $this->last_name;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getMobileNumber(): ?string
    {
        return $this->mobile_number;
    }

    public function getCompany(): ?array
    {
        return $this->company;
    }

    public function getJobTitle(): ?string
    {
        return $this->job_title;
    }

    public function getMedium(): ?string
    {
        return $this->medium;
    }

    public function getIdLead(): ?int
    {
        return $this->id_lead;
    }

    public function getSubscriptionStatus(): ?array
    {
        return $this->subscription_status;
    }

    public function getEmails(): ?array
    {
        return $this->emails;
    }

    public function getWorkNumber(): ?string
    {
        return $this->work_number;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function getZipcode(): ?string
    {
        return $this->zipcode;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function getLeadStageId(): ?int
    {
        return $this->lead_stage_id;
    }

    public function getLeadReasonId(): ?int
    {
        return $this->lead_reason_id;
    }

    public function getLeadSourceId(): ?int
    {
        return $this->lead_source_id;
    }

    public function getOwnerId(): ?int
    {
        return $this->owner_id;
    }

    public function getCampaignId(): ?int
    {
        return $this->campaign_id;
    }

    public function getKeyword(): ?string
    {
        return $this->keyword;
    }

    public function getTimeZone(): ?string
    {
        return $this->time_zone;
    }

    public function getFacebook(): ?string
    {
        return $this->facebook;
    }

    public function getTwitter(): ?string
    {
        return $this->twitter;
    }

    public function getLinkedin(): ?string
    {
        return $this->linkedin;
    }

    public function getTerritoryId(): ?int
    {
        return $this->territory_id;
    }

    public function getDeal(): ?array
    {
        return $this->deal;
    }

    public function getCreatedAt(): ?string
    {
        return $this->created_at;
    }

    public function getUpdatedAt(): ?string
    {
        return $this->updated_at;
    }

    public function getInclude(): ?string
    {
        return $this->include;
    }

    public function getSelectedIds(): array
    {
        if (empty($this->selected_ids)) {
            throw FreshsalesSdkException::forDataNotProvided(ExceptionMessages::SELECTED_IDS);
        }

        return $this->selected_ids;
    }

    public function getIdView(): ?int
    {
        return $this->id_view;
    }
}
