<?php

namespace Pabon\FreshsalesSdk\Support;

use Pabon\FreshsalesSdk\Constants\Operations;
use Pabon\FreshsalesSdk\Exceptions\ParserException;
use Pabon\FreshsalesSdk\Parsers\BulkDeleteLeadsParser;
use Pabon\FreshsalesSdk\Parsers\CloneLeadParser;
use Pabon\FreshsalesSdk\Parsers\ConvertLeadParser;
use Pabon\FreshsalesSdk\Parsers\CreateLeadParser;
use Pabon\FreshsalesSdk\Parsers\DeleteLeadParser;
use Pabon\FreshsalesSdk\Parsers\ForgetLeadParser;
use Pabon\FreshsalesSdk\Parsers\ListAllActivitiesParser;
use Pabon\FreshsalesSdk\Parsers\ListAllLeadFieldsParser;
use Pabon\FreshsalesSdk\Parsers\ListAllLeadsParser;
use Pabon\FreshsalesSdk\Parsers\UpdateLeadParser;
use Pabon\FreshsalesSdk\Parsers\ViewLeadParser;
use PlacetoPay\Tangram\Entities\BaseSettings;

class ParserManager
{
    protected const OPERATIONS_PARSERS = [
        Operations::CREATE_LEAD => CreateLeadParser::class,
        Operations::VIEW_LEAD => ViewLeadParser::class,
        Operations::DELETE_LEAD => DeleteLeadParser::class,
        Operations::FORGET_LEAD => ForgetLeadParser::class,
        Operations::BULK_DELETE_LEADS => BulkDeleteLeadsParser::class,
        Operations::UPDATE_LEAD => UpdateLeadParser::class,
        Operations::CONVERT_LEAD => ConvertLeadParser::class,
        Operations::CLONE_LEAD => CloneLeadParser::class,
        Operations::LIST_ALL_LEADS => ListAllLeadsParser::class,
        Operations::LIST_ALL_LEAD_FIELDS => ListAllLeadFieldsParser::class,
        Operations::LIST_ALL_ACTIVITIES => ListAllActivitiesParser::class,
    ];

    protected array $parsers = [];
    protected BaseSettings $settings;

    public function __construct(BaseSettings $settings)
    {
        $this->settings = $settings;
    }

    public function getParser(string $operation)
    {
        $this->validateOperation($operation);

        if (! isset($this->parsers[$operation])) {
            $this->createParser($operation);
        }

        return $this->parsers[$operation];
    }

    protected function createParser(string $operation): void
    {
        $parserName = self::OPERATIONS_PARSERS[$operation];
        $this->parsers[$operation] = new $parserName($this->settings);
    }

    /**
     * @throws ParserException
     */
    protected function validateOperation(string $operation): void
    {
        if (! isset(self::OPERATIONS_PARSERS[$operation])) {
            throw ParserException::invalidOperation($operation);
        }
    }
}
